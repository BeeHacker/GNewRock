- 👋 Hi, I’m Ildar @GNewRock
- 👀 I’m interested in C/C++, Haskell, Python, Docker
- 🌱 I’m currently learning Haskell
- 💞️ I’m looking to collaborate on Elegant Software
<!--- 📫 How to reach me ...--->

![GNewRock's GitHub stats](https://github-readme-stats.vercel.app/api?username=gnewrock&show_icons=true&theme=merko)

<!---
[![github stats](https://github-readme-stats.vercel.app/api?username=GNewRock&show_icons=true)](https://github.com/anuraghazra/github-readme-stats)  
--->

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=GNewRock&layout=compact&theme=maroongold)](https://github.com/anuraghazra/github-readme-stats)

<!---[![GNewRock's gnewrock stats](https://github-readme-stats.vercel.app/api/gnewrock?username=gnewrock&theme=dark)](https://github.com/anuraghazra/github-readme-stats)--->


<!---
GNewRock/GNewRock is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
